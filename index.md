---
title: Waste Material Catalogue Project DDP
description: Use of CNN to estimate, and reuse construction waste
author: Elder and Miguel 
keywords: waste,CNN,reuse
url: https://rwth-crmasters-sose22.gitlab.io/shared-runner-group/waste-material-reuse/#1 
marp: true
image: 
---

# Waste Material Catalogue
## Key Words

- Waste
- Convolutional Neural Networks
- Quantity Estimation

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_dsiZfgbtpE9eraOUxh5F01yXN_NsIZ_7yw&usqp=CAU" width="175" height="175"> 

---

> <img src="https://bloximages.newyork1.vip.townnews.com/observer-reporter.com/content/tncms/assets/v3/editorial/2/94/294028bb-c7b6-51d4-a9be-0c454f8c897a/5a27bd8a969ff.image.jpg?crop=1577%2C887%2C11%2C0&resize=1577%2C887&order=crop%2Cresize" width="330" height="210"> 

- The Construction Industry Waste
- UK 120 million tonnes of waste/year - 44% ends in landfill.
- USA 29%.
- Globally 35%.
- Australia - 7 million tonnes
- Part of the waste are diverted away from landfill
- Recycling involves a lot of transportation and processing
- Our Goals
 
---



<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected stories

## Personal experience in :building_construction:

> <img src="https://woimacorporation.com/wp-content/uploads/2020/10/Wood-waste-and-other-construction-waste-on-a-pile-WOIMA-Corporation.jpg" width="350" height="200">   <img src="https://images.adsttc.com/media/images/5100/56e7/b3fc/4b45/7c00/000a/medium_jpg/Construction_Debris.jpg?1414597365" width="300" height="200">

- Colleagues experiences in Colombia: disposal through a chute and manual selection by each contractor
- Complementary application: reuse of excendent materials
- Concrete reuse in highways and footing
- Reuse of masonry in sewer wells


---

# Mindmap

::: fence

@startuml

@startmindmap

*[#Orange] Construction and Demolition
**[#Orange] Solid Waste Material
***[#Orange] Information collection
****[#Orange] Identification with CNN method
***** Separation
****** Robot
****** Manually
****** Mechanically
****** Eletromagnetically
*****[#Orange] Quantity Estimation
******[#Orange] Reusable
*******[#Orange] Short-term storage
********[#Orange] Cloud Storage information and Spreed
*********[#Orange] Application
*********[#Orange] Website
********* Construction Magazines
********* Catalogue
********* Software (BIM)
******* Instant Reuse
****** Not Reusable
******* Transportation
******* Bury
**** Machine Learning
**** Multiplayer perception
**** Support vector machine
*** Local shredder
**** Shredded and distributed
**** Shredded and instant reuse
*** Transport to a specilalized company to shredder
**** Hight energy consuming
**** Machinery
**** Large space dedicated to a specific activity
**** Time consuming
**** Remains of waste material
** Organic Waste Material
*** ...
** Liquid Waste Material
*** ...

@endmindmap

@enduml

:::

---

## Convolutional Neural Network (CNN) concept

The Classisfication of Construction Waste Material Using Deep CNN

<img src="https://hackernoon.com/hn-images/1*SL8FESMwzSy6QTrcIzcRYw.png" width="250" height="100"> 

- Study conducted in Australia - Automated Classification of Construction & Demolition Waste (C&DW)
- Environmental, Productivity and Costs Benefits 
- Objective of identifying categories of waste in a construction bin
- Emphasizes use frequency of CNNs in for identification and classification of C&DW
- Use of cameras for residue identification and classification
- Single and mixed waste classification 
- Waste were classified with very high accuracy 

---

## Waste Material Catalogue

So, what to do with data?

- Recycling involves a lot of transportation and processing
- Shortcut all the logistic
- Directly reuse of materials
- Collect information about these materials early enough
- Share it in an online platform
- Connect or reconnect material with people or companies 

---

## Waste Material Catalogue

How can we do it?

- Device connected to the CNN
- **Quantification** and **Qualification** of the available materials
- **Where** and **When** the material will be available
- Interactive map of available materials
- Deal between individuals or between companies
- Materials no longer become waste and contractors disposals are reduced

---

# Open-source & hardware secondary research

[Tensorflow](https://github.com/tensorflow/tensorflow): Open source to use CNN

- This repository allows us to work with different programming languages, and it has been used for develop other ideas with similar configuration

---

# Locked concept

- Attempt to use TensorFlow to process the information about construction waste and create a platform to reuse the material.
- Relate the offer of reusable materials with the demand in the construction market.
- Build and train a model according to the current state of the region.
